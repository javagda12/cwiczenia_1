USE cwiczenia_javagda12;

# usuwam tabele przed jej dodaniem
DROP TABLE IF EXISTS books;

# Komentarze do kodu
CREATE TABLE books (
	title VARCHAR(255),
    author VARCHAR(255),
    published DATE NOT NULL,
    isbn CHAR(17) NOT NULL,
    category VARCHAR(255),
    page_count INT NOT NULL,
    publisher VARCHAR(255) DEFAULT 'Helion',
    price DECIMAL(10, 2)
);

DROP TABLE IF EXISTS author;

CREATE TABLE author(
	firstname VARCHAR(255),
    lastname VARCHAR(255)
);


ALTER TABLE books MODIFY COLUMN price DECIMAL(10,3);

-- Tabela 
ALTER TABLE books MODIFY COLUMN title VARCHAR(255) NOT NULL;
ALTER TABLE books MODIFY COLUMN author VARCHAR(255) NOT NULL;

ALTER TABLE books MODIFY COLUMN isbn CHAR(17) NOT NULL; -- zle  
ALTER TABLE books MODIFY COLUMN isbn CHAR(17) UNIQUE;     -- zle 
ALTER TABLE books MODIFY COLUMN isbn CHAR(17) UNIQUE NOT NULL;     -- dobrze

ALTER TABLE books MODIFY COLUMN publisher VARCHAR(255) DEFAULT 'nieznana';

-- Przy komendzie alter column nie musimy podawać całej definicji kolumny, a wyłącznie elementy które chcemy zmienić
ALTER TABLE books ALTER COLUMN publisher SET DEFAULT 'nieznana';

-- Dopisanie nowej kolumny z wartością defaultową 0 
ALTER TABLE books ADD COLUMN in_stock INTEGER DEFAULT 0;
