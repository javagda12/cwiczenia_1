USE cwiczenia_javagda12;

INSERT INTO books (title, author, published, isbn, category, page_count, publisher, price) VALUES ('Spring w akcji. Wydanie IV', 'Craig Walls', '2015-08-13', '978-83-283-0849-7', 'programowanie java', 624, 'Helion', 89.00);
-- INSERT INTO books VALUES ('Spring w akcji. Wydanie IV', 'Craig Walls', '2015-08-13', '978-83-283-0849-7', 'programowanie java', 624, 'Helion', 89.00);
-- INSERT INTO books (title, author, published, isbn, category, page_count, publisher, price) VALUES ('MySQL. Vademecum profesjonalisty', 'Paul DuBois', '2014-03-28', '978-83-246-8146-4', 'bazy danych', 1216, 'Helion', 149.00);
INSERT INTO books (title, author, published, isbn, category, page_count, publisher, price) VALUES ('MySQL. Vademecum profesjonalisty', 'Paul DuBois', '2014-03-28', '978-83-246-8146-4', 'bazy danych', 1216, 'Helion', 149.00);

SELECT * FROM books;