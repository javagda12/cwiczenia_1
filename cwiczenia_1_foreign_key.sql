USE cwiczenia_javagda12;

# usuwam tabele przed jej dodaniem
DROP TABLE IF EXISTS books;
DROP TABLE IF EXISTS author;

CREATE TABLE author(
	id INT,
	firstname VARCHAR(255),
    lastname VARCHAR(255),
    PRIMARY KEY(id)
) DEFAULT CHARACTER SET utf8 COLLATE utf8_polish_ci;

# Komentarze do kodu
CREATE TABLE books (
	id INT PRIMARY KEY,
	title VARCHAR(255),
    author_id INT,
    published DATE NOT NULL,
    isbn CHAR(17) NOT NULL UNIQUE,
    category VARCHAR(255),
    page_count INT NOT NULL,
    publisher VARCHAR(255) DEFAULT 'Helion',
    price DECIMAL(10, 2),
    FOREIGN KEY(author_id) REFERENCES author(id) 
    ON UPDATE SET null
) DEFAULT CHARACTER SET utf8 COLLATE utf8_polish_ci;

-- 2/3 AUTORÓW
-- TRUNCATE TABLE author;
-- TRUNCATE TABLE books;
insert into author values(1, 'Jan', 'Kochanowski');
insert into author values(2, 'Adam', 'Mickiewicz');
insert into author values(3, 'Juliusz', 'Słowacki');

INSERT INTO books VALUES (1, 'Fraszki', 1, '1584-01-01', '19921992199219921', 'Wiersze', 100, 'Wydawnictwo Naukowe PWN', 100.0);
INSERT INTO books VALUES (2, 'Fraszki', 1, '1564-02-09', '19921992199219901', 'Wiersze', 100, 'Wydawnictwo Naukowe PWN', 152.0);
INSERT INTO books VALUES (3, 'Sonety Krymskie', 2, '1784-06-11', '19921996199219921', 'Wiersze', 100, 'Wydawnictwo N-n-naukowe PWN', 13.5);
INSERT INTO books VALUES (4, 'Oda do mlodosci', 2, '1882-02-21', '19921292199219921', 'Wiersze', 100, 'Wydawnictwo N-n-naukowe PWN', 99.2);
INSERT INTO books VALUES (5, 'Dziady', 2, '1711-02-02', '19951992199219921', 'Wiersze', 100, 'Wydawnictwo N-n-naukowe PWN', 56.3);

-- 5/6 KSIAZEK