-- 1. Wypisz wszystkie kategorie.
-- 2. Wypisz wszystkich pracownikow.
-- 3. Wypisz wszystkie imiona pracownikow.
-- 4. Wypisz wszystkich klientow.
-- 5. Wypisz wszystkie nazwiska klientow
-- 6. Wypisz wszystkich pracownikow zarabiajacych wiecej niz 12000
-- 7. Wypisz wszystkich ktorzy zarabiaja od 1000 do 5000
-- 8. Wypisz wszystkie dzialy
-- 9. Wypisz wszystkich dostawcow 
-- 10. Wypisz wszystkie stany z ktorych sa dostawcy.
-- 11. Wypisz wszystkie produkty
-- 12. *Wypisz wszystkie nazwy produktów
-- 13a. *Zmodyfikuj tabele produkty, dodaj kolumne ilość
-- 13b. *Posortuj produkty po ilości i wypisz wszystkich nazwa i ilość

SELECT * FROM categories;

SELECT * FROM employees;

SELECT first_name AS imiona_pracownikow FROM employees;

SELECT * FROM customers;

SELECT last_name AS nazwiska_klientow FROM employees;

SELECT * FROM employees WHERE salary > 12000;

SELECT * FROM employees WHERE salary BETWEEN 1000 AND 5000;

SELECT * FROM departments;

SELECT * FROM suppliers;

SELECT DISTINCT state AS stany FROM suppliers;

SELECT * FROM products;

SELECT DISTINCT product_name AS nazwy_produktow FROM products;

SET SQL_SAFE_UPDATES = 0;

UPDATE products SET quantity = 3 WHERE product_name = 'Banana';
UPDATE products SET quantity = 5 WHERE product_name = 'Apple';
UPDATE products SET quantity = 1 WHERE product_name = 'Bread';
UPDATE products SET quantity = 1 WHERE product_name = 'Kleenex';

SELECT product_name, quantity FROM products ORDER BY quantity ASC;