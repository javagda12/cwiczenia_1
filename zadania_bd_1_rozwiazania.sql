-- 1. Wypisz wszystkie kategorie.
SELECT * FROM categories;

-- 2. Wypisz wszystkich pracownikow.
SELECT * FROM employees;

-- 3. Wypisz wszystkie imiona pracownikow.
SELECT first_name as 'Imie' FROM employees;

-- 4. Wypisz wszystkich klientow.
SELECT * FROM customers;

-- 5. Wypisz wszystkie nazwiska klientow
SELECT last_name FROM customers;
 
 -- 6. Wypisz wszystkich pracownikow zarabiajacych wiecej niz 12000
 SELECT * FROM employees WHERE salary > 12000;
 
 -- 7. Wypisz wszystkich ktorzy zarabiaja od 1000 do 5000
 SELECT * FROM employees WHERE salary BETWEEN 1000 AND  5000;
 
 -- 8. Wypisz wszystkie dzialy
 SELECT * FROM departments;
 
 -- 9. Wypisz wszystkich dostawcow 
 SELECT * FROM suppliers;
 
 -- 10. Wypisz wszystkie stany z ktorych sa dostawcy.
SELECT distinct(state) from suppliers;

-- 11. Wypisz wszystkie produkty
SELECT * FROM products;

-- 12. *Wypisz wszystkie nazwy produktów
SELECT product_name FROM products;

-- 13a. *Zmodyfikuj tabele produkty, dodaj kolumne ilość
ALTER TABLE products ADD COLUMN quantity INT DEFAULT 0;
SELECT * FROM PRODUCTS;

UPDATE products SET quantity = 3 WHERE product_name = 'Banana';
UPDATE products SET quantity = 5 WHERE product_name = 'Apple';
UPDATE products SET quantity = 1 WHERE product_name = 'Bread';
UPDATE products SET quantity = 8 WHERE product_name = 'Kleenex';

-- SELECT product_name, quantity FROM products ORDER BY quantity ASC;
SELECT t.product_name, t.quantity FROM (select * from products order by product_name DESC) as t ORDER BY t.quantity ASC;
